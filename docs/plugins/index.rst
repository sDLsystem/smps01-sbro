.. _plugins:

sbroETL Plugins
===============

Plugins are blocks of code that can be used in batch mode or streaming mode, which are deployed by framework components (see :ref:`FrameworkClasses`), and implement the version of a specific algorithm generating controlled output metrics. Such allows to dynamically choose the best plugin for a particular problem solution. Plugins are i87 binary files with execution controlled by the sbroETL system and audit managed by block chain technology. Plugins may have associated royalties.


**List of Project Plugins**

.. toctree::
    :maxdepth: 1

   plugin_fbip01_path_order
   plugin_fbip02_path_validation
   plugin_fbip03_locstats
   plugin_fbip04_end2start
   plugin_fbip05_stats 
   plugin_fbip06_state
   plugin_fbip07_nodeBottleneck
   plugin_fbip08_node2data
   plugin_fbip09_graphs
   plugin_fbip10_report
   plugin_x03_MJ_FBIPpredict
   plugin_x04_DC1_FEFFann
   plugin_x05_DC_FEFFtrain
   plugin_x06_DC_FEFFpredict
   plugin_x07_stepStats

**List of sbroETL Plugins**

.. toctree::
   :maxdepth: 1

   plugin_rc01_processRootCause
   plugin_tb01_generalClassifier_train
   plugin_tb02_generalClassifier_predict
   plugin_tb03_generalRegressor_train
   plugin_tb04_generalRegressor_predict
   plugin_x01_GFTfit
   plugin_x02_GFTpredict
   