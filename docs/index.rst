Project SM0101
==============

The Smart Analytics Platform with Image Similarity Search (SM0101) project intends to apply analytic techniques (based on machine learning, deep learning, and big data) to improve data usability with special focus on the development of a image-based mold search system.   


------------

HIGHLIGHTS
----------

------------

- Dashboarding development



------------

**TABLE OF CONTENT**

------------

.. toctree::
   :maxdepth: 3
   :caption: PROJECT INFORMATION

   D00-Scheme
   D01-Scope
   D03-MissionPurpose
   D04-VisionAmbition
   D05-WorkAreas

.. toctree::
   :maxdepth: 3
   :caption: SM0101.PPS1 (WorkArea)

   SM0101.PPS1/InnovativeCharacteristics
   SM0101.PPS1/D06-Scientific-Outputs
   SM0101.PPS1/D07-WorkPackages
   SM0101.PPS1/D18-Products-Core
   SM0101.PPS1/D27-UseCases-Core

