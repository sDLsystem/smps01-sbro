D04-VisionAmbition
==================

- **Description**: The SAPISS project aims to anticipate the future by developing user-friendly, immersive and supportive technologies in industrial production environments. Through a coordinated strategy between industrial partners and research centers, for the development of relevant technologies and tools that make industrial processes more efficient, less impactful on workers' health, reduce risk and prepare and adapt both processes and human resources for the new digitization paradigms – Industry 4.0.

