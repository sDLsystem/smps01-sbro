PRODUCT STRUCTURE

## Schemes on the SDL project sdlxxxxxx-{projectRef}.{workareaRef}.{workpackageRef}.{productRef}-{usecaseRef}

--- sys: Base framework (inFiles: sbroETL-sys.yml, class: FOSM/project) -- mandatory
1. [exec sys_install/M01] (Re)create default system tables
2. [...] (Re)create datain table "DinINST" [institutional format]
3. [...] (Re)reate dataout table "DoutINST" [institutional format]
4. [...] (Re)create datain table "DinSDL" [CDF/sys format]
5. [...] (Re)create dataout table "DoutSDL" [CDF/sys format]
6. [...] Create tables of other frameworks (if needed)
7. [exec sys_config/M01] Set system configurations
8. [exec sys_docs/M02] Generate product documentation
9. [up] Load SDL specific services, sbroETL variants "{*}F|{b,k}D{in,out}G" services and sbroBRT services
10. [down] Unload all services create by up
11. [exec sys_main/M01] Execution of product maintenance tasks
12. [run] exec sys_docs

--- cronF: Cron framework (inFiles: sbroFMK-cronF.yml, class: FOSM/project) 
1. [exec cronF_main01] Call an ETL execution via "./run-sbro.sh ..."
2. [run] scheduler start

--- mesF: Measure framework (inFiles: sbroFMK-mesF.yml, class: DMAIC/project) 
1. [exec mesF_calcVars/M04] Calculate variables
3. [exec mesF_vapdata{UC}] From DinSDL_tmp, validate, aggregate, and prepare data into DinSDL 
4. [exec mesF_studyData] Execution of the study framework with auxiliar documentation generation
5. [run] forEach UC exec mesF_get{UC}; exec mesF_vapdata{UC}; done; exec mesF_studyData

--- rc01F: Root Cause approach 1 framework (inFiles: sbroFMK-rc01F.yml, class: DMAIC/project) 
1. [yaml] Define corresponding DMAIC/PDCA concepts
2. [exec rc01F_tagsImpact] ...
3. [run] forEach KPI exec rc01F_tagsImpact

--- rc02F: Root Cause approach 2 framework (inFiles: sbroFMK-rc02F.yml, class: DMAIC/project) 
1. [yaml] Define corresponding DMAIC/PDCA concepts
2. [exec rc02F_main] Call an ETL execution via "./run-sbro.sh"
3. [run] forEach KPI exec rc02F_main

--- sdF: Solutions determination framework (inFiles: sbroFMK-sdF.yml, class: DMAIC/project) 
1. [yaml] Define corresponding DMAIC/PDCA concepts
2. [exec sdF_datasets] (Re)create table datasets
3. [...] Fill the datasets table
4. [exec sdF_docs] Generate report R1
5. [run] exec sdF_datasets; forEach DS exec sdF_benchmark; exec sdF_RRShap; exec custeringGrouping done; exec sdF_docs

forEach NN
| --- pdca{NN}F: PDCA {NN} framework (inFiles: sbroFMK-PDCA{NN}F.yml, class: DMAIC/project) 
| 1. [yaml] Define corresponding DMAIC/PDCA concepts
| 2. [exec pdca{NN}F_plan] ...
| 3. [exec pdca{NN}F_do] ...
| 4. [exec pdca{NN}F_check] ...
| 5. [exec pdca{NN}F_act] ...
| 6. [exec pdca{NN}F_main] ...
| 7. [run] exec pdca{NN}F_main
| 
| --- f{NN}F: Fit framework (inFiles: sbroFMK-f{NN}F.yml, class: DMAIC/project, class: DMAIC/project)
| 1. [exec sys_install/sys] (Re)create needed tables
| 2. [exec f{NN}F_main] Execution of the fit framework
| 3. [run] exec f{NN}F_main
| 
| --- ma{NN}F: Model application {NN} framework (inFiles: sbroFMK-ma{NN}F.yml, class: DMAIC/project)
| 1. [exec sys_install/sys] (Re)create needed tables
| 2. [exec ma{NN}F_main] Validate the existence of dataout f{NN}F tables
| 3. [...] Generate prediction microservices
| 4. [...] Activate prediction microservices
| 5. [run] exec pF_main

--- ovF Overall validation framework (inFiles: sbroFMK-ovF.yml, class: DMAIC/project)
1. [exec sys_install/sys] (Re)create needed tables
2. [exec ovF_main] Execution of the overall validation tasks
3. [run] exec ovF_main


--- DVEDAG: Data Validation and Exploratory Data Analysis framework (inFiles: sbroFMK-DVEDAG.yml, class: GATEWAYS/project)
1. [exec DVEDAG_main] For each locationID 
2. [...] Filter current data in DinSDL
3. [...] Call the needed plugins to validate the data
3. [...] Call the needed plugins to generate the correswponding reports
4. [run] exec DVEDAG_main

--- bDinG-{inst}{NN}: Batch datain gateway framework (inFiles: sbroFMK-bDinG-{inst}{NN}.yml, class: GATEWAYS/project)
1. [cron bDinG-{inst}{NN}_main] Chooses the correct csv input file and inject into DinINST 
2. [...] Filter current data in DinINST and mark it
3. [...] For marked data, validate data, reject problematic data and trigger alerts
4. [...] For marked data, transform data into DinSDL (calculating the needed variables)
5. [...] For marked data, remove rows 
6. [...] Call any needed execution via "./run-sbro.sh"
7. [run] exec scheduler start

--- bDoutG-{inst}{NN}: Batch dataout gateway framework (inFiles: sbroFMK-bDoutG-{inst}{NN}.yml, class: GATEWAYS/project)
1. [cron bDoutG_{inst}{NN}_main] Chooses the csv output framework for the active institution
2. [...] Filter current data in DoutSDL and mark it
2. [...] For marked data, transform data into DoutINST
3. [...] For marked data, remove rows
4. [...] Send DoutINST data to external institutional sources
5. [...] Remove all rows from DoutINST
6. [run] exec scheduler start

--- kDinG-{inst}{NN}: Streaming datain gateway framework (inFiles: sbroFMK-kDinG-{inst}{NN}.yml+sbroBRT.yml//datain, class: GATEWAYS/project)
1. [exec kDinG-{inst}{NN}_main] sql2exec kafka2timeseries
2. [run] exec kafka start 
 
--- kDoutG-{inst}{NN}: Streaming dataout gateway framework (inFiles: sbroFMK-kDoutG-{inst}{NN}.yml+sbroBRT.yml//dataout, class: GATEWAYS/project)
1. [exec kDoutG_{inst}{NN}_main] sql2exec timeseries2kafka
2. [run] exec kafka start 

--- mainF Main product framework (inFiles: sbroFMK-mainF.yml, class: DMAIC/project)
1. [exec sys_install/sys] (Re)create needed tables
2. [exec mainF_main] Execution of the main product tasks
3. [run] exec mainF_main

--- monF Validation framework (inFiles: sbroFMK-monF.yml, class: GATEWAYS/project)
1. [exec sys_install/sys] (Re)create needed tables
2. [exec monF_main] Execution of the validation frameworks
3. [run] exec monF_main

## REMARKS

RMK01: When calling "exec_framework XXX", the sbroETL system tests if there exists sbroFMK-XXX.yml and merge it with sbroETL.yml on load
