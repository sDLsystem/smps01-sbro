SM0101.PPS1 Frameworks
======================

.. [DADF] **Frameworks for data analytics and dashboarding**

- Project Frameworks:

 - **DADF_sys**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`DADF_sys`

  - **Description**:

   Framework for Product-UseCase services management
   
   
 - **DADF_bDinG-SM01**:

  - **Status Phase**: Draft

  - **Common Data Format**: :ref:`DADF_bDinG-SM01`

  - **Description**:

   Read the [SM]_ file format for **Molds** and inject it to the internal analytics database.
   
   
.. [ISSF] **Frameworks for image similarity search**

- Project Frameworks:

 - **ISSF_sys**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`ISSF_sys`

  - **Description**:

   Framework for Product-UseCase services management
   
   
 - **ISSF_bDinG-SM01**:

  - **Status Phase**: Draft

  - **Common Data Format**: :ref:`ISSF_bDinG-SM01`

  - **Description**:

   Read the [SM]_ file format for **Molds** and inject it to the internal analytics database.
   
   
 - **ISSF_fImageSimilarityG**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`ISSF_fImageSimilarityG`

  - **Description**:

   From molds data, this framework finds the best model for find image similarity.
   
   
 - **ISSF_maImageSimilarityG**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`ISSF_maImageSimilarityG`

  - **Description**:

   In real-time and using ISSF_fImageSimilarityG model, this framework find the set of closest images to a given image 
   
   
