D18-Products-Core
=================

------------

SM0101.PPS1.W1.DADF
~~~~~~~~~~~~~~~~~~~

------------

.. [SM0101.PPS1.W1.DADF] **Frameworks for data analytics and dashboarding**

- **shortDescription**: TBD

- **D19-EstimatedGains&Costs**: TBD

- **D20-Technology**:

 - **coordinator**: [UA.ER]_ [UA.AB]_

 - **producers**: [UA]_

 - **consumers**: [SM]_

- **D21-ScientificCoordination**: [UA.AB]_

- **D22-DeploymentCoordination**: [SM.JM]_ [UA.ER]_

- **D23-Features**: TBD

- **D24-KPIdesc**:

 - **k000**: minimize | TBD

- **D25-KPIvalidator**:

 - **k000**: TBD

------------

SM0101.PPS1.W1.ISSF
~~~~~~~~~~~~~~~~~~~

------------

.. [SM0101.PPS1.W1.ISSF] **Frameworks for image similarity search**

- **shortDescription**: TBD

- **D19-EstimatedGains&Costs**: TBD

- **D20-Technology**:

 - **coordinator**: [UA.ER]_

 - **producers**: [UA]_

 - **consumers**: [SM]_

- **D21-ScientificCoordination**: [UA.ER]_

- **D22-DeploymentCoordination**: [SM.JM]_ [UA.ER]_

- **D23-Features**: TBD

- **D24-KPIdesc**:

 - **k000**: minimize | TBD

- **D25-KPIvalidator**:

 - **k000**: TBD

