Innovative Characteristics
==========================

.. [IC01] **Image-based molds (web/mobile) search engine**

- **Unit**: n/a

- **Market Status**: New in the market

- **Project Objectives**: Use machine learning and deep learning to develop a mold image similarity sbroETL SDL and the human interface.

- **Relative Significance**: 60%

- **Achivements**:

 - **Scientific**: TBD

 - **Industrial**: TBD

.. [IC02] **Data analytics and visualization of molds data**

- **Unit**: n/a

- **Market Status**: Custom

- **Project Objectives**: Improve the analytics and create efficient dashboards for molds data

- **Relative Significance**: 20%

- **Achivements**:

 - **Scientific**: TBD

 - **Industrial**: TBD

.. [IC03] **Consultancy/training on topics related to Big Data and Industry 4.0**

- **Unit**: n/a

- **Market Status**: Custom

- **Project Objectives**: Develop a custom made training program to fit company's needs for up-to-date technology education

- **Relative Significance**: 20%

- **Achivements**:

 - **Scientific**: TBD

 - **Industrial**: TBD

