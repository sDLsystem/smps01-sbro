D01-Scope
=========

- **coordination**: [SM]_

- **main_scientific_partner**: [UA]_

- **dateStart**: 01-04-2022

- **dateEnd**: 30-05-2023

- **partnersSuffix**:

.. [SM] Grupo Simoldes

.. [SMSA] Simoldes Aços SA

.. [SMMDA] Moldes de Azeméis SA

.. [SMIMA] Indústria de Moldes de Azeméis SA

.. [SMIGM] IGM - Indústria Global de Moldes SA

.. [UA] `Universidade de Aveiro <https://www.ua.pt>`_

- **projectSuffix**: SM0101

